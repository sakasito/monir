import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class ListPage extends StatefulWidget {
  const ListPage({super.key});

  @override
  State<ListPage> createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  List contentList = [
    'Account',
    'Saved Addresses',
    'Credit / Debit Cards',
    'Languages/ เปลียนภาษา',
    'Live Chat',
    'Find a store',
    'Terms & Conditions',
    'Privacy Statement'
  ];

  AppBar renderAppbar() {
    return AppBar(
      toolbarHeight: 100,
      backgroundColor: Colors.white,
      title: Column(
        children: const [
          Text(
            'More',
            style: TextStyle(
                color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(0, 20, 0, 10),
            child: Text(
              'Hi, Jens Tofte',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Text(
            'jens@email.com',
            style: TextStyle(color: Colors.grey, fontSize: 12),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: renderAppbar(),
        body: ListView.builder(
          itemCount: contentList.length,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.fromLTRB(12, 20, 12, 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('${contentList[index]}'),
                  const Icon(
                    Icons.navigate_next,
                    color: Colors.black54,
                    size: 25.0,
                  )
                ],
              ),
            );
          },
        ));
  }
}
